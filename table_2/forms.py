from django.forms import ModelForm, TextInput, NumberInput, DateTimeInput

from .models import Table2


class Table2Form(ModelForm):
    class Meta:
        model = Table2
        fields = [ 'cam_id', 'damage', 'reason', 'planned_recovery', 'recovery', 'adress']

        widgets = {
            "cam_id": NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'Идентификатор камеры'
            }),
            "damage": DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder': 'Дата выхода из строя',
                'format': '%d.%m.%Y %H:%M',
                'type': 'datetime-local',

            }),
            "reason": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Причина не функционирования',
                'list': 'reason',
            }),
            "planned_recovery": DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder': 'Планируемая дата восстановления',
                'format': '%d.%m.%Y %H:%M',
                'type': 'datetime-local',
            }),
            "recovery": DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder': 'Планируемая дата восстановления',
                'format': '%d.%m.%Y %H:%M',
                'type': 'datetime-local',
            }),
            "adress": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Адрес места установки',
            }),
        }
