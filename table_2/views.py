import csv
import datetime
#from weasyprint import HTML
import tempfile
from django.db.models import Q

from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, redirect
from django.template.loader import render_to_string

from .models import Table2, spr_reasons
from .forms import Table2Form
from city import shared

@shared.my_permission_required('table_2.view_table2')
# Вывод таблицы
def table(request):
    if request.method == 'POST': # Если нажата кнопка "Фильтровать" получаем введенные даты и сохраняем в сессию
        from_date, to_date = shared.get_post_date_filter(request)
    else: # иначе получаем даты из функции
        from_date, to_date = shared.get_date_filter(request)

    # Получаем из таблицы записи за интересующие даты, а также камеры у которых состояние не восстановлено. Исключаем при этом удаленные строки
    table_ = Table2.objects.filter(Q(damage__range=(from_date, to_date)) | Q(recovery__isnull=True) | Q(recovery__gte=datetime.datetime.now())).exclude(deleted=True)

    if not request.user.is_superuser: # Если не администратор то записи только города пользователя
        unit = shared.get_unit(request.user)
        userlist = shared.get_userlist_by_unit(unit)
        table_ = table_.filter(user__in=userlist)

    table_ = table_.order_by('damage', 'planned_recovery')

    for row in table_: # В цикле всем записям проставляем город по пользователю
        user = row.user
        row.unit = shared.get_unit(user).name
        # и выставляем значение для класса строки
        row.status = 'not_recovery'
        if row.recovery and row.recovery <= datetime.datetime.now():
            row.status = 'recovery'

    # Переводим даты в строки по нужному формату
    to_date = to_date.strftime("%Y-%m-%d")
    from_date = from_date.strftime("%Y-%m-%d")
    data = {
        'table': table_,
        'from_date': from_date,
        'to_date': to_date
    }
    return render(request, 'table/table.html', data)


# Создание новой записи
def create(request):
    errors = ''
    if request.method == 'POST': # Если нажата кнопка "Создать" то сохраняем запись в таблицу
        form = Table2Form(request.POST)
        if form.is_valid():
            form.data.id = None
            form1 = form.save(commit=False)
            form1.user = request.user
            form1.date = datetime.date.today().strftime('%Y-%m-%dT%H:%M')
            form1.save()
            return redirect('table_2')
        else:
            errors = form.errors
    else: # Подготавливаем форму для создания записи
        dict_ = {
            'date': datetime.date.today().strftime('%Y-%m-%dT%H:%M'),
        }
        form = Table2Form(initial=dict_)

    options = spr_reasons.objects.order_by('reason_name')

    data = {
        'form': form,
        'errors': errors,
        'options': options,
    }
    return render(request, "table/create.html", data)


# Удаление строки
def delete(request, id):
    try:
        # По id получаем запись
        record = Table2.objects.get(id=id)
        if request.method == 'POST': # Если нажата кнопка "Удалить" - удаляем запись из таблицы
            record.deleted = True
            record.save()
            return redirect('table_2')
        else: # Подготавливаем форму для удаления записи из таблицы
            dict_ = {
                'cam_id': record.cam_id,
                'damage': record.damage.strftime('%Y-%m-%dT%H:%M'),
                'reason': record.reason,
                'planned_recovery': record.planned_recovery.strftime('%Y-%m-%dT%H:%M'),
                'adress': record.adress,
            }

        data = {
            'values': dict_,
        }
        return render(request, "table/delete.html", data)
    except Table2.DoesNotExist:
        return HttpResponseNotFound("<h2>Строка не найдена</h2>")


# Создание дубликата записи
def duplicate(request, id):
    try:
        errors = ''
        if request.method == 'POST': # Если на форме нажата кнопка "Сохранить" сохраняем запись в таблицу
            form = Table2Form(request.POST)
            if form.is_valid():
                form.data.id = None
                form1 = form.save(commit=False)
                form1.user = request.user
                form1.date = datetime.date.today().strftime('%Y-%m-%dT%H:%M')
                form1.save()
                return redirect('table_2')
            else:
                errors = form.errors
        else: # Подготавливаем форму для измения записи
            record = Table2.objects.get(id=id)
            dict_ = {
                'cam_id': record.cam_id,
                'damage': record.damage.strftime('%Y-%m-%dT%H:%M'),
                'reason': record.reason,
                'planned_recovery': record.planned_recovery.strftime('%Y-%m-%dT%H:%M'),
                'adress': record.adress,
            }
            form = Table2Form(initial=dict_)

        options = spr_reasons.objects.order_by('reason_name')

        data = {
            'title': '(через дублирование записи)',
            'form': form,
            'errors': errors,
            'options': options,
        }
        return render(request, "table/edit.html", data)
    except Table2.DoesNotExist:
        return HttpResponseNotFound("<h2>Строка не найдена</h2>")


# Редактирование таблицы
def edit(request, id):
    try:
        errors = ''
        record = Table2.objects.get(id=id)
        if request.method == 'POST': # Если на форме нажата кнопка "Сохранить" сохраняем запись в таблицу
            form = Table2Form(request.POST, instance=record)
            if form.is_valid():
                form1 = form.save(commit=False)
                form1.user = request.user
                form1.save()
                return redirect('table_2')
            else:
                errors = form.errors
        else: # Подготавливаем форму для измения записи
            dict_ = {
                'cam_id': record.cam_id,
                'damage': record.damage.strftime('%Y-%m-%dT%H:%M'),
                'reason': record.reason,
                'planned_recovery': record.planned_recovery.strftime('%Y-%m-%dT%H:%M'),
                'adress': record.adress,
            }
            form = Table2Form(initial=dict_)

        options = spr_reasons.objects.order_by('reason_name')

        data = {
            'title': '',
            'form': form,
            'errors': errors,
            'options': options,
        }
        return render(request, "table/edit.html", data)
    except Table2.DoesNotExist:
        return HttpResponseNotFound("<h2>Строка не найдена</h2>")


# Экспорт в csv
def csv_export(request):
    response = HttpResponse(content_type='text/csv')

    writer = csv.writer(response, delimiter=';')
    writer.writerow(['date', 'str_num', 'cam_id', 'damage', 'reason', 'planned_recovery', 'adress'])

    table_ = Table2.objects.all().values_list('date', 'str_num', 'cam_id', 'damage', 'reason', 'planned_recovery',
                                              'adress')
    for row in table_:
        writer.writerow(row)

    now = datetime.datetime.now()
    dt = now.strftime("%Y%m%d_%H%M%S")
    response['Content-Disposition'] = 'attachment; filename="export_table2_{dt}.csv"'.format(dt=dt)

    return response

# Экспорт в pdf
def pdf_export(request):
    # Данные модели """Создание pdf."""
    table_ = Table2.objects.all().order_by('-date')
    # Обработка шаблона
    html_string = render_to_string('table/generate.html', {'table': table_})
    html = HTML(string=html_string)
    result = html.write_pdf()

    # Создание http ответа
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=list_people.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())

    return response
