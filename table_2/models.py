from django.db import models
from django.contrib.auth.models import User
from report.models import Units


class Table2(models.Model):
    id = models.IntegerField('Идентификатор ', primary_key=True)
    user = models.ForeignKey(User, null=True, blank=True,
                             on_delete=models.SET_NULL)
    date = models.DateTimeField('Дата подачи данных')
    str_num = models.IntegerField('Номер строки', null=True, blank=True)
    cam_id = models.IntegerField('Идентификатор камеры')
    damage = models.DateTimeField('Дата выхода из строя')
    reason = models.CharField('Причина не функционирования', max_length=50)
    planned_recovery = models.DateTimeField('Планируемая дата восстановления')
    recovery = models.DateTimeField('Дата восстановления',null=True, blank=True)
    adress = models.CharField('Адрес места установки', max_length=50, null=True, blank=True)
    deleted = models.BooleanField('Флаг скрытия записи', default=False)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Таблица 2'
        verbose_name_plural = 'Таблица 2'


class guide_camera(models.Model):
    marka_name = models.CharField('Марка камеры', max_length=30)
    model_name = models.CharField('Модель', max_length=30)
    def __str__(self):
        return self.marka_name + ' - ' + self.model_name

    class Meta:
        verbose_name = 'Камера'
        verbose_name_plural = 'Камеры'


class spr_reasons(models.Model):
    reason_name = models.CharField('Причины не функционирования', max_length=60)

    def __str__(self):
        return self.reason_name

    class Meta:
        verbose_name = 'Причины не функционирования'
        verbose_name_plural = 'Причины не функционирования'
