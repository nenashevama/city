# Generated by Django 3.1.4 on 2021-01-10 18:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0007_delete_unit'),
        ('table_2', '0017_remove_table2_unit'),
    ]

    operations = [
        migrations.AddField(
            model_name='table2',
            name='unit',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='report.units'),
        ),
    ]
