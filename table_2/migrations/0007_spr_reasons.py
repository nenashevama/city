# Generated by Django 3.1.4 on 2020-12-21 08:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('table_2', '0006_articles_adress'),
    ]

    operations = [
        migrations.CreateModel(
            name='spr_reasons',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reason_name', models.CharField(max_length=30, verbose_name='Причины не функционирования')),
            ],
        ),
    ]
