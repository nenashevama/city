from django.contrib import admin
from .models import Table2, guide_camera, spr_reasons


admin.site.register(Table2)

admin.site.register(guide_camera)
admin.site.register(spr_reasons)
