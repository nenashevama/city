from django.urls import path
from . import views

# Шаблон файла URLS для таблиц
# меняем только суффикс (номер) таблицы например '_2' на '_3'
urlpatterns = [
    path('', views.table, name='table_2'),
    path('create', views.create, name='create_2'),
    path('edit/<int:id>/', views.edit, name='edit_2'),
    path('delete/<int:id>/', views.delete, name='delete_2'),
    path('duplicate/<int:id>/', views.duplicate, name='duplicate_2'),
    path('csv', views.csv_export, name='csv_2'),
    path('pdf', views.pdf_export, name='pdf_2'),
]
