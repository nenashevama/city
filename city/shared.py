import datetime
from report.models import UsersProfiles
from django.contrib.auth.decorators import permission_required

def get_unit(user):
    profil = UsersProfiles.objects.get(user=user)
    return profil.unit

def get_post_date_filter(request):
    to_date = datetime.datetime.strptime(request.POST['to_date'], '%Y-%m-%d').date()
    from_date = datetime.datetime.strptime(request.POST['from_date'], '%Y-%m-%d').date()
    set_session_date_filter(request, from_date, to_date)
    return from_date, to_date

def set_session_date_filter(request,from_date, to_date):
    request.session['to_date'] = to_date.strftime("%Y-%m-%d")
    request.session['from_date'] = from_date.strftime("%Y-%m-%d")
    request.session.modified = True

def get_date_filter(request):
    if 'to_date' in request.session:  # Пытаемся получить даты из сессии
        to_date = request.session.get('to_date')
        from_date = request.session.get('from_date')
        to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d").date()
        from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d").date()
    else:  # Если сессии еще нет то ставим значения по умолчанию (с сегодняшнего по завтрашний день)
        to_date = datetime.date.today() + datetime.timedelta(days=1)
        from_date = datetime.date.today()
    return from_date, to_date

def my_permission_required(perm):
    return permission_required(perm, '/accounts/login')

def get_userlist_by_unit(unit):
    userlist = UsersProfiles.objects.filter(unit=unit).values_list('user')
    return userlist