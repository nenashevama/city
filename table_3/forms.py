from django.forms import ModelForm, TextInput, NumberInput, DateTimeInput, Select

from .models import Table3, SprIncidentType
from report.models import  Units



class Table3Form(ModelForm):
    class Meta:
        model = Table3

        fields = ['date', 'cam_id', 'incident_type', 'description', 'dt', 'place', 'action', 'action_result']

        incident_types = SprIncidentType.objects.all().values_list('id','incident_type')

        widgets = {
            "cam_id": NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'Идентификатор камеры'
            }),
            "date": DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder': 'Дата подачи данных',
                'format': '%d.%m.%Y %H:%M',
                'type': 'datetime-local',
            }),
            "incident_type": Select(
                choices=incident_types,
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Идентификатор типа происшествия',
                }
            ),
            "description": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Описание происшествия',
                'list': 'description',
            }),
            "dt": DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder': 'Дата/время происшествия',
                'format': '%d.%m.%Y %H:%M',
                'type': 'datetime-local',
            }),
            "place": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Адрес места установки',
            }),
            "action": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Принятые меры',
                'list': 'action',
            }),
            "action_result": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Результат принятых мер',
                'list': 'action_result',
            })
        }
