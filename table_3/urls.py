from django.urls import path
from django.conf.urls import url
from table_3 import views

urlpatterns = [
    path('', views.table, name='table_3'),
    path('create', views.create, name='create_3'),
    path('edit/<int:id>/', views.edit, name='edit_3'),
    path('delete/<int:id>/', views.delete, name='delete_3'),
    path('duplicate/<int:id>/', views.duplicate, name='duplicate_3'),
    path('csv', views.csv_export, name='csv_3'),
    path('pdf', views.pdf_export, name='pdf_3'),
    path('report', views.report, name='report_3'),
    ]