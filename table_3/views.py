import csv
import datetime
#from weasyprint import HTML
import tempfile

from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.db.models import Q

from django.utils import timezone
import pytz

from .models import Table3, SprIncidentType
from .forms import Table3Form
from city import shared
from report.models import Units
from table_2.models import Table2


class EmptyClass:
    info = 'Пустой класс'

@shared.my_permission_required('table_3.view_table3')
# Create your views here.
def table(request):
    if request.method == 'POST': # Если нажата кнопка "Фильтровать" получаем введенные даты и сохраняем в сессию
        from_date, to_date = shared.get_post_date_filter(request)
    else: # иначе получаем даты из функции
        from_date, to_date = shared.get_date_filter(request)

    # Получаем из таблицы записи за интересующие даты. Исключаем при этом удаленные строки
    table = Table3.objects.filter(date__range = (from_date, to_date)).exclude(deleted=True)

    if not request.user.is_superuser:  # Если не администратор то записи только города пользователя
        unit = shared.get_unit(request.user)
        userlist = shared.get_userlist_by_unit(unit)
        table = table.filter(user__in=userlist)\

    table.order_by('-date')

    for row in table: # В цикле всем записям проставляем город по пользователю
        user = row.user
        row.unit = shared.get_unit(user).name

    # Переводим даты в строки по нужному формату
    to_date = to_date.strftime("%Y-%m-%d")
    from_date = from_date.strftime("%Y-%m-%d")

    data = {
        'table': table,
        'from_date': from_date,
        'to_date': to_date
    }
    return render(request, 'table.html', data)


def create(request):
    errors = ''

    if request.method == 'POST':
        form =Table3Form(request.POST)
        if form.is_valid():
            form.data.id = None
            form1 = form.save(commit=False)
            form1.user = request.user
            form1.save()
            return redirect('table_3')
        else:
            errors = form.errors
    else:
        dict = {
            'date': datetime.date.today().strftime('%Y-%m-%dT%H:%M'),
        }
        form = Table3Form(initial=dict)

    data = {
        'form': form,
        'errors': errors,
    }
    return render(request, "create.html", data)

def edit(request, id):
    try:
        errors = ''
        record = Table3.objects.get(id=id)
        if request.method == 'POST': # Если нажата кнопка "Создать" то сохраняем запись в таблицу
            form = Table3Form(request.POST, instance=record)
            if form.is_valid():
                form1 = form.save(commit=False)
                form1.user_id = request.user
                form1.save()
                return redirect('table_3')
            else:
                errors = form.errors
        else: # Подготавливаем форму для создания записи
            dict = {
                'date': record.date.strftime('%Y-%m-%dT%H:%M'),
                'str_num': record.str_num,
                'cam_id': record.cam_id,
                'incident_type': record.incident_type,
                'description': record.description,
                'dt': record.dt.strftime('%Y-%m-%dT%H:%M'),
                'place': record.place,
                'action': record.action,
                'action_result': record.action_result,
            }
            form = Table3Form(initial=dict)


        data = {
            'title': '',
            'form': form,
            'errors': errors,
        }
        return render(request, "edit.html", data)
    except Table3.DoesNotExist:
        return HttpResponseNotFound("<h2>Строка не найдена</h2>")

def duplicate(request, id):
    try:
        errors = ''
        if request.method == 'POST': # Если на форме нажата кнопка "Сохранить" сохраняем запись в таблицу
            form = Table3Form(request.POST)
            if form.is_valid():
                form.data.id = None
                form1 = form.save(commit=False)
                form1.user = request.user
                form1.date = datetime.date.today().strftime('%Y-%m-%dT%H:%M')
                form1.save()
                return redirect('table_3')
            else:
                errors = form.errors
        else:
            record = Table3.objects.get(id=id)
            dict = {
                'date': record.date.strftime('%Y-%m-%dT%H:%M'),
                'str_num': record.str_num,
                'cam_id': record.cam_id,
                'incident_type': record.incident_type,
                'description': record.description,
                'dt': record.dt.strftime('%Y-%m-%dT%H:%M'),
                'place': record.place,
                'action': record.action,
                'action_result': record.action_result,
            }
            form = Table3Form(initial=dict)


        data = {
            'title': '(через дублирование записи)',
            'form': form,
            'errors': errors,
        }
        return render(request, "edit.html", data)
    except Table3.DoesNotExist:
        return HttpResponseNotFound("<h2>Строка не найдена</h2>")


# Удаление строки
def delete(request, id):
    try:
        # По id получаем запись
        record = Table3.objects.get(id=id)
        if request.method == 'POST':
            record.deleted = True
            record.save()
            return redirect('table_3')
        else:
            dict = {
                'date': record.date.strftime('%Y-%m-%dT%H:%M'),
                'str_num': record.str_num,
                'cam_id': record.cam_id,
                'incident_type': record.incident_type,
                'description': record.description,
                'dt': record.dt.strftime('%Y-%m-%dT%H:%M'),
                'place': record.place,
                'action': record.action,
                'action_result': record.action_result,
            }

        data = {
            'values': dict,
        }
        return render(request, "delete.html", data)
    except Table3.DoesNotExist:
        return HttpResponseNotFound("<h2>Строка не найдена</h2>")


def csv_export(request):
    response = HttpResponse(content_type='text/csv')

    writer = csv.writer(response, delimiter=';')
    writer.writerow(['date', 'cam_id', 'incident_type', 'description', 'dt','place','action','action_result'])

    table = Table3.objects.all().values_list(
        'date', 'cam_id', 'incident_type', 'description', 'dt', 'place', 'action', 'action_result'
        )
    for row in table:
        writer.writerow(row)

    now = datetime.datetime.now()
    dt = now.strftime("%Y%m%d_%H%M%S")
    response['Content-Disposition'] = 'attachment; filename="export_table3_{dt}.csv"'.format(dt=dt)

    return response


def pdf_export(request):

# Данные модели """Создание pdf."""
    table = Table3.objects.all().order_by('-date')
# Обработка шаблона
    html_string = render_to_string('generate.html', {'table': table})
    html = HTML(string=html_string)
    result = html.write_pdf()

# Создание http ответа
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=list_people.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())

    return response

def report(request):
    units = Units.objects.all().order_by('name') # Получили все подразделения
    table = []
    total = EmptyClass()
    total.cam_count = 0
    total.work_cam = 0
    total.pdd = 0
    total.dtp = 0
    total.nop = 0
    total.all = 0
    from_date, to_date = shared.get_date_filter(request)
    for unit in units: # Цикл по подразделениям
        row = EmptyClass()
        row.unit = unit.name
        row.cam_count = unit.cam_count
        users = shared.get_userlist_by_unit(unit)
        damaged_table = Table2.objects.filter(damage__range=(from_date, to_date)) # Выбрали поврежденными камерами по датам фильтра
        damaged_table = damaged_table.filter(Q(recovery__isnull=True) | Q(recovery__gte=to_date)) # Выбрали не восстановленные камеры и камеры с датой востановления больше равно последней датф фильтра дат
        damaged_table = damaged_table.exclude(deleted=True) # Исключили удаленные записи
        damaged_table = damaged_table.filter(user__in=users) # Выбрали записи по пользователям интересующего подразделения
        damaged = damaged_table.count() # Получили кол-во записей
        row.work_cam = row.cam_count - damaged
        pdd = SprIncidentType.objects.get(incident_type='ПДД') # получили тип ПДД
        dtp = SprIncidentType.objects.get(incident_type='ДТП')  # получили тип ДТП
        nop = SprIncidentType.objects.get(incident_type='НОП')  # получили тип НОП
        incidents = Table3.objects.filter(dt__range=(from_date, to_date)) # Выбрали происшествия за даты
        incidents = incidents.exclude(deleted=True) # Исключили удаленные записи
        incidents = incidents.filter(user__in=users) # Выбрали записи по пользователям интересующего подразделения
        row.pdd = incidents.filter(incident_type=pdd).count() # Выбрали по типу "ПДД" и получили кол-во
        row.dtp = incidents.filter(incident_type=dtp).count() # Выбрали по типу "ДТП" и получили кол-во
        row.nop = incidents.filter(incident_type=nop).count() # Выбрали по типу "НОП" и получили кол-во
        row.all = row.pdd + row.dtp + row.nop
        table.append(row)
        total.cam_count += row.cam_count
        total.work_cam += row.work_cam
        total.pdd += row.pdd
        total.dtp += row.dtp
        total.nop += row.nop
        total.all += row.all

    from_dt = to_date - + datetime.timedelta(days=7)
    to_dt = to_date
    incidents = Table3.objects.filter(dt__range=(from_dt, to_dt)) # Выбрали происшествия за даты
    incidents = incidents.exclude(deleted=True) # Исключили удаленные записи
    #incidents = incidents.filter(user__in=users) # Выбрали записи по пользователям интересующего подразделения

    total.week = incidents.count() # Кол-во проишествий за неделю

    from_dt = from_date.replace(day=1)
    to_dt = to_date
    incidents = Table3.objects.filter(dt__range=(from_dt, to_dt)) # Выбрали происшествия за даты
    incidents = incidents.exclude(deleted=True) # Исключили удаленные записи
    #incidents = incidents.filter(user__in=users) # Выбрали записи по пользователям интересующего подразделения

    total.month = incidents.count() # Кол-во проишествий с начала месяца

    from_dt = from_date.replace(day=1).replace(month=1)
    to_dt = to_date
    incidents = Table3.objects.filter(dt__range=(from_dt, to_dt)) # Выбрали происшествия за даты
    incidents = incidents.exclude(deleted=True) # Исключили удаленные записи
    #incidents = incidents.filter(user__in=users) # Выбрали записи по пользователям интересующего подразделения

    total.year = incidents.count() # Кол-во проишествий с начала года

    total.repair = 'Нет данных'

    # Переводим даты в строки по нужному формату
    to_date = to_date.strftime("%d.%m.%Y")
    from_date = from_date.strftime("%d.%m.%Y")

    data = {
        'table': table,
        'total': total,
        'to_date': to_date,
        'from_date' : from_date,
    }
    return render(request, "report.html", data)