from django.db import models
from django.contrib.auth.models import User
from report.models import Units

class SprIncidentType(models.Model):
    incident_type = models.CharField('Тип происшествия', max_length=60)

    def __str__(self):
        return self.incident_type

    class Meta:
        verbose_name = 'Тип происшествия'
        verbose_name_plural = 'Типы происшествия'

# class Incidents(models.Model):
#     id = models.IntegerField('Идентификатор ', primary_key=True)
#     user_id = models.ForeignKey(User, null=True, blank=True,
#                                 on_delete=models.SET_NULL)
#     unit = models.ForeignKey(Units, null=True, blank=True,
#                               on_delete=models.SET_NULL)
#     date = models.DateTimeField('Дата подачи данных')
#     str_num = models.IntegerField('Номер строки', null=True, blank=True)
#     cam_id = models.IntegerField('Идентификатор камеры')
#     incident_type = models.ForeignKey(SprIncidentType, null=True, blank=True, on_delete=models.SET_NULL)
#     description = models.CharField('Описание', max_length=50)
#     dt = models.DateTimeField('Время')
#     place = models.CharField('Место', max_length=50, null=True, blank=True)
#     action = models.CharField('Принятые меры', max_length=50, null=True, blank=True)
#     action_result = models.CharField('Результат принятых мер', max_length=50, null=True, blank=True)
#
#     def __str__(self):
#         return str(self.id)
#
#     class Meta:
#         verbose_name = 'Таблица 3'
#         verbose_name_plural = 'Таблица 3'


class Table3(models.Model):
    id = models.IntegerField('Идентификатор ', primary_key=True)
    user = models.ForeignKey(User, null=True, blank=True,
                                on_delete=models.SET_NULL)
    date = models.DateTimeField('Дата подачи данных')
    str_num = models.IntegerField('Номер строки', null=True, blank=True)
    cam_id = models.IntegerField('Идентификатор камеры')
    incident_type = models.ForeignKey(SprIncidentType, null=True, blank=True, on_delete=models.SET_NULL)
    description = models.CharField('Описание', max_length=50)
    dt = models.DateTimeField('Время')
    place = models.CharField('Место', max_length=50, null=True, blank=True)
    action = models.CharField('Принятые меры', max_length=50, null=True, blank=True)
    action_result = models.CharField('Результат принятых мер', max_length=50, null=True, blank=True)
    deleted = models.BooleanField('Флаг скрытия строки', default=False)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Таблица 3'
        verbose_name_plural = 'Таблица 3'
