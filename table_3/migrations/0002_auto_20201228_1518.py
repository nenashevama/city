# Generated by Django 3.1.4 on 2020-12-28 12:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('table_3', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='incidents',
            name='action',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Принятые меры'),
        ),
        migrations.AlterField(
            model_name='incidents',
            name='action_result',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Результат принятых мер'),
        ),
    ]
