from django.urls import path, include
from . import views, admin

urlpatterns = [
    path('', views.index, name='home'),
    path('about', views.about, name='about'),
    path('create', views.create, name='create'),
    path('accounts/', include('django.contrib.auth.urls')),
]